# [Cypress.io](https://www.cypress.io/) vs [Playwright](https://playwright.dev/) vs [Selenium](https://www.selenium.dev/selenium/docs/api/javascript/index.html)

## Cypress

### Requirements

- [Node.js](https://nodejs.org/en/) with npm

### Installation

- clone the repository
- run `npm install cypress`

### Supported browsers

- Chromium (Chrome, Edge)
- Firefox
- Electron

### Run

`npx cypress open` and follow it up in the test runner
or from CLI: `npx cypress run Cypress` where 'Cypress' is directory with tests. Full set of commands: https://docs.cypress.io/guides/guides/command-line#Commands

## Playwright

### Requirements

- [Node.js](https://nodejs.org/en/) with npm
- if on linux, you will need to be on [Ubuntu 20.4](https://ubuntu.com/download/desktop), otherwise you will have problems due to missing libraries

### Installation

- clone the repository
- run `npm install`

    - this will install all dependecies
    - and also downloads all compatible browser binaries (chromium, firefox, webkit)

- set up `.dotenv` config file, which should have this structure

    ```
    BROWSER = "chromium"
    HEADLESS = "false"
    VIEWPORT = {"width": 1920, "height": 1080}
    ```

### Supported browsers

- chromium (Chrome, Edge)
- webkit (safari)
- firefox

### Run

- to run playwright tests on Windows

    ```
    // set up ENV var to playwright
    $env:FRAMEWORK="PLAYWRIGHT"
    // then run the npm script
    npm run-script test-playwright
    ```

- to run playwright tests on linux (Ubuntu)

    ```
    // just execute
    FRAMEWORK=PLAYWRIGHT npm run-script test-playwright
    ```

## Selenium

### Requirements

- have [chromedriver](https://chromedriver.chromium.org/downloads) and [geckodriver](https://github.com/mozilla/geckodriver/releases) downloaded and added to PATH. As per [recommendation on Selenium documentation page](https://www.selenium.dev/selenium/docs/api/javascript/index.html), on local environment we do not use selenium-standalone-server.

### Installation

- clone the repository
- run `npm install`
- set up `.dotenv` config file - see above

### Supported browsers

- Chrome
- Firefox

### Run

- to run selenium tests on Windows

    ```
    // set up ENV var to selenium
    $env:FRAMEWORK="SELENIUM"
    // then run the npm script
    npm run-script test-selenium
    ```

- to run selenium tests on linux (Ubuntu)

    ```
    // just execute
    FRAMEWORK=SELENIUM npm run-script test-selenium
    ```

## Reporting

- console `spec` reporter - native reporting tool for [Mocha.js](https://mochajs.org/)
- [mochawesome](https://www.npmjs.com/package/mochawesome) HTML reporting plugin

## Manual execution of single script

Used mainly for running single script from `**/pure scripts/**` folders

### On Ubuntu
```
FRAMEWORK=PLAYWRIGHT node_modules/.bin/mocha --spec src/playwright/spec/pure\ scripts/tc01_consentBannerDisplayedOnFirstVisit.spec.ts
```

### On Windows 10 with PowerShell (NOT Windows PowerShell!)

#### Set env variable to run given framework

```
// for playwright
$env:FRAMEWORK="PLAYWRIGHT"

// for selenium
$env:FRAMEWORK="SELENIUM"
```

#### Run given script

```
.\node_modules\.bin\mocha --spec '.\src\playwright\spec\pure scripts\tc_02_contactFormNotSentWithoutEmail.spec.ts'
```