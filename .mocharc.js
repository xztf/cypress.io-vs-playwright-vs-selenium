"use strict";
const os = require("os");
require("dotenv").config();
// either SELENIUM or PLAYWRIGHT
const FRAMEWORK =
  process.env.FRAMEWORK !== undefined ? process.env.FRAMEWORK : "PLAYWRIGHT";
const mochaFixturePathPlaywright = "src/playwright/modules/mochaFixture.ts";
const mochaHookPathPlaywright = "src/playwright/modules/mochaHook.ts";
const mochaHookPathSelenium = "src/selenium/modules/mochaHook.ts";

const requireItems =
  FRAMEWORK === "PLAYWRIGHT"
    ? [
        "ts-node/register",
        mochaFixturePathPlaywright,
        mochaHookPathPlaywright,
        "mochawesome/register",
      ]
    : ["ts-node/register", mochaHookPathSelenium, "mochawesome/register"];

module.exports = {
  require: requireItems,
  reporter: ["spec", "mochawesome"],
  timeout: 60000,
  ui: "bdd",
  recursive: true,
  parallel: true,
  jobs: os.cpus().length / 2,
  slow: 15000,
};
