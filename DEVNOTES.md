# hooks 

- hooks for SELENIUM or PLAYWRIGHT will be run according to ENV variable `FRAMEWORK`. IF it is not set, then PLAYWRIGHT is used and run.

# ENV params

- FRAMEWORK - SELENIUM || PLAYWRIGHT
- BROWSER - chromium || webkit || firefox
- HEADLESS - true || false
- VIEWPORT - eg. {width: "1920", height: "1080"} - this is string, which will be parsed by JSON.parse()