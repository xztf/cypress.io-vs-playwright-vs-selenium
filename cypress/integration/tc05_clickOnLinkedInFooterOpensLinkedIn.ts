/// <reference types="cypress" />

describe('TC05 - click on icon opens LinkedIn', () => {

  beforeEach( () => {
    cy
      .visit('/')
  });

  it('click on footer LinkedIn icon navigated to LinkedIn page', () => {
    cy
      .get('[title=LinkedIn]')
      .should('have.attr', 'href').and('equal', 'https://www.linkedin.com/company/tesena')
    cy
      .get('[title=LinkedIn]')
      .click()  
    cy
      .log('Working with multiple tabs/domains is not possible in Cypress')
    cy
      .url()
      .should('include', 'www.linkedin.com/')
  });     
});