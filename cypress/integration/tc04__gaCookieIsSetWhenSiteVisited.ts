/// <reference types="cypress" />

describe('TC04 - _ga cookie is set when site visited', () => {

    beforeEach( () => {
      cy
        .visit('/')
    });

    it('_ga cookies are set for domain www.tesena.com', () => {
      cy
        .getCookie('_ga')
        .then((cookies) => {
            expect(cookies).not.to.be.undefined
            expect(cookies).to.be.an('object')
            expect(cookies.value).not.to.be.empty
        })
    });
});