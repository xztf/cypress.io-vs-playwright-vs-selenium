/// <reference types="cypress" />

describe('TC09 - Dropdown selector works', () => {

  beforeEach( () => {
    cy
      .visit('/en/integration-test-analyst')
    cy
      .get('select[aria-label="Job Title"]')
      .select('Test Automation Engineer')
  });

  it("Option 'Test Automation Engineer' was selected", () => {
    cy
      .get('select[aria-label="Job Title"]')
      .should('have.value', 'Test Automation Engineer')
  });    
});