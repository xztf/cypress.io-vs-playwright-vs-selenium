/// <reference types="cypress" />

describe('TC06 - YouTube iframe is in the DOM', () => {

  beforeEach( () => {
    cy
      .visit('/en/insights')
  });
    
  // cypress cannot access content of an iframe by default
  // (browser blocks javascript access to antoher domain's element
  // for security reasons)
  // this can be bypassed by setting
  // 'chromeWebSecurity' to 'false' in cypress.json config file

  it('iframe has youtube content', () => {
    cy
      .get('iframe[src="//www.youtube.com/embed/erHSAzUPiIQ?=erHSAzUPiIQ')
      .should('be.visible')
      .its('0.contentDocument').should('exist')
      .its('body').should('not.be.undefined')
      .then(cy.wrap)
      .find('[href="https://www.youtube.com/watch?v=erHSAzUPiIQ"]')
      .should('have.text', 'Testing in the Digital Age; AI Makes the Difference | Rik Marselis | Tesena Fest 2019')
  });
})