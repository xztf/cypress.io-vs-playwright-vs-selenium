describe('Assertion - element visibility', () => {

  it('TC01 - Consent banner is displayed', () => {
    cy
      .visit('/')
    cy
      .get('button.btn-confirm')
      .should('be.visible')
  });
});