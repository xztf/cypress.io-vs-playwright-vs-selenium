/// <reference types="cypress" />

describe('TC07 - Alert popup is displayed', () => {

  beforeEach( () => {
    cy
      .visit('https://the-internet.herokuapp.com/javascript_alerts')
  });

   it('dialog Alert was displayed', () => {
    const stub = cy.stub()

    cy
      .on('window:alert', stub)
    cy
      .contains('Click for JS Alert')
      .click()
      .then(() => {
        expect(stub.getCall(0)).to.be.calledWith('I am a JS Alert')
      })
  })
});