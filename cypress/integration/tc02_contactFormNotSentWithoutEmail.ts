/// <reference types="cypress" />

describe('TC02 - form will not send unless ​email is provided', () => {
  
  beforeEach( () => {
    cy
      .visit('/contact')
    cy
      .get('#field-name')
      .type('cypress tester')
    cy
      .get('#field-message')
      .type('cypress message')
    cy
      .get('[type="submit"]')
      .click()
    });

  it("Send button is still visible", () => {
    cy
      .get('[type="submit"]')
      .should('be.visible')
  });

  it("Missing email error message is displayed", () => {
    cy
      .get('.field-message--error')
      .should('be.visible')
  });
});

