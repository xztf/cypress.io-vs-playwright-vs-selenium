/// <reference types="cypress" />

describe('TC08 - Adding file for upload works', () => {

  beforeEach( () => {
    cy
      .visit('en/integration-test-analyst')
    cy
      .fixture('testCV.png').as('CV');
    cy
      .get('[data-l="Your CV"]').then(function (el) {
      const blob = Cypress.Blob.base64StringToBlob(this.CV, 'image/png')

      const file = new File([blob], 'testCV.png', { type: 'image/png' })
      const list = new DataTransfer()

      list.items.add(file)
      const myFileList = list.files

      el[0].files = myFileList
      el[0].dispatchEvent(new Event('change', { bubbles: true }))
    })
  });
  it('Name of the uploaded file is displayed', () => {
    cy
      .contains('testCV.png').should('be.visible')
  });    
});