/// <reference types="cypress" />

describe('TC03 - GA Requests are sent if consents are provided', () => {
  
  beforeEach( () => {
    cy
      .intercept('GET',/^(https:\/\/)(www\.google-analytics\.com\/)(\w?)(\/?)(collect\?).*$/)
      .as('gAnalytics')
    cy
      .visit('/')
    cy
      .get('.btn-confirm')
      .click()
  });
        
  it("GA requests are being sent", () => {
    cy
      .wait('@gAnalytics')
      .then((response) => {
        expect(response.response.statusCode).to.eq(200)        
      })
  });
});