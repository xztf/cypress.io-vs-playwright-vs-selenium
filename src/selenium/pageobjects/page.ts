import {
  ThenableWebDriver,
  WebElement,
  until,
  By,
  IWebDriverCookie,
} from "selenium-webdriver";
import { Options } from "selenium-webdriver";
/**
 * Generic page object. Never instantiate thic class itself.
 * It should be always extended by page objects for concrete
 * pages or their components.
 * All methods, getters and props reusable across multiple page objects
 * should be here.
 */
export class SeleniumGenericPage {
  /**
   * Webdriver instance. See {@link https://www.selenium.dev/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_ThenableWebDriver.html | link}
   * for exact definition and methods available.
   */
  driver: ThenableWebDriver;
  /** Xpath selector of the Accept Button of the Consent banner element. */
  consentBannerAcceptBttnSelector: string;
  /**
   * Cookies found in the browser context
   */
  cookies: Array<IWebDriverCookie>;
  /**
   * Linked In link xpath selector in the footer.
   */
  linkFooterLinkedInSelector: string;
  /** Generic timeout for `until` methods */
  timeout: number;

  constructor(driver: ThenableWebDriver) {
    this.driver = driver;
    this.timeout = 30000;
    this.cookies = [];
    this.linkFooterLinkedInSelector =
      "//a[contains(@class, 'social-linkedin')]";
    this.consentBannerAcceptBttnSelector =
      "//button[contains(@class, 'btn-confirm')]";
  }
  /**
   * Gets consent banner accept bttn WebElement
   */
  get consentBannerAcceptBttn(): WebElement {
    return this.driver.wait(
      until.elementIsVisible(
        this.driver.findElement(By.xpath(this.consentBannerAcceptBttnSelector))
      ),
      this.timeout
    );
  }
  /**
   * Gets link footer linked in WebElement
   */
  get linkFooterLinkedIn(): WebElement {
    return this.driver.wait(
      until.elementLocated(By.xpath(this.linkFooterLinkedInSelector)),
      this.timeout
    );
  }
  /**
   * Opens browser and navigates to specific url.
   * Wrapper for `ThenableWebdriver.get()` method
   * @param url - url to navigate to.
   */
  get(url: string): void {
    this.driver.get(url);
  }
  /**
   * Waits for given time in miliseconds before continuing.
   * Defaults to 5000 ms.
   * @param timeout - how long to wait in ms.
   */
  wait(timeout = 5000): void {
    this.driver.sleep(timeout);
  }
  /**
   * Gets cookie from opened page by given name.
   * @param name - name of the cookie
   */
  async getCookie(name: string): Promise<void> {
    const options: Options = this.driver.manage();
    this.cookies.push(await options.getCookie(name));
  }
  /**
   * Clicks consent banner accept bttn
   */
  async clickConsentBannerAcceptBttn(): Promise<void> {
    const el: WebElement = this.consentBannerAcceptBttn;
    await el.click();
    this.driver.wait(until.elementIsNotVisible(el), this.timeout);
  }
  /**
   * Scrolls to element
   * @param element - `WebElement` to scroll to
   */
  scrollToElement(element: WebElement): void {
    this.driver.executeScript("arguments[0].scrollIntoView();", element);
  }
  /**
   * Navigates to linked in via clicking on
   * footer linked in icon.
   */
  async navigateToLinkedInViaFooter(): Promise<void> {
    const firstWindowHandle: string = await this.driver.getWindowHandle();
    const el: WebElement = this.linkFooterLinkedIn;
    this.scrollToElement(el);
    await el.click();
    await this.driver.wait(
      async () => (await this.driver.getAllWindowHandles()).length === 2,
      this.timeout
    );
    const handles: Array<string> = await this.driver.getAllWindowHandles();
    handles.forEach(async (handle) => {
      if (handle !== firstWindowHandle) {
        await this.driver.switchTo().window(handle);
      }
    });
    await this.driver.wait(until.titleContains("LinkedIn"), this.timeout);
  }
}
