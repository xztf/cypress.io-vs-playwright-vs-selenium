import { ThenableWebDriver, until, WebElement } from "selenium-webdriver";
import { SeleniumGenericPage } from "../pageobjects/page";
import { By } from "selenium-webdriver";

export class SeleniumContactUs extends SeleniumGenericPage {
  /** URL of the page. */
  url: string;
  /**
   * Contact form submit bttn xpath selector
   */
  contactFormSubmitBttnSelector: string;
  /**
   * Contact form name input xpath selector
   */
  contactFormNameInputSelector: string;
  /**
   * Contact form text area xpath selector
   */
  contactFormTextAreaSelector: string;
  /**
   * Contact form email input error msg xpath selector
   */
  contactFormEmailInputErrorMsgSelector: string;

  constructor(driver: ThenableWebDriver) {
    super(driver);
    this.url = "https://www.tesena.com/en/contact";
    this.contactFormSubmitBttnSelector = "//button[@type='submit']";
    this.contactFormNameInputSelector = "//input[contains(@id, 'field-name')]";
    this.contactFormTextAreaSelector =
      "//textarea[contains(@id, 'field-message')]";
    this.contactFormEmailInputErrorMsgSelector =
      "//div[@id='field-wrapper-email']//div[contains(@class, 'field-message--error')]";
  }
  /**
   * Gets contact form submit bttn WebElement
   */
  get contactFormSubmitBttn(): WebElement {
    return this.driver.wait(
      until.elementLocated(By.xpath(this.contactFormSubmitBttnSelector)),
      super.timeout
    );
  }
  /**
   * Gets contact form name input WebElement
   */
  get contactFormNameInput(): WebElement {
    return this.driver.wait(
      until.elementLocated(By.xpath(this.contactFormNameInputSelector)),
      super.timeout
    );
  }
  /**
   * Gets contact form text area WebElement
   */
  get contactFormTextArea(): WebElement {
    return this.driver.wait(
      until.elementLocated(By.xpath(this.contactFormTextAreaSelector)),
      super.timeout
    );
  }
  /**
   * Gets contact form email input error msg WebElement
   */
  get contactFormEmailInputErrorMsg(): WebElement {
    return this.driver.wait(
      until.elementLocated(
        By.xpath(this.contactFormEmailInputErrorMsgSelector)
      ),
      super.timeout
    );
  }
  /** Opens the page via `this.url` property */
  visit(): void {
    super.get(this.url);
  }
  /**
   * Fills name in contact form input field
   * @param name - name to be filled
   */
  fillNameInContactForm(name: string): void {
    const el: WebElement = this.contactFormNameInput;
    super.scrollToElement(el);
    el.sendKeys(name);
  }
  /**
   * Fills message in contact form text area
   * @param message - message to be filled
   */
  fillMessageInContactForm(message: string): void {
    const el: WebElement = this.contactFormTextArea;
    super.scrollToElement(el);
    el.sendKeys(message);
  }
  /**
   * Clicks contact form send button
   */
  clickContactFormSendBttn(): void {
    const el: WebElement = this.contactFormSubmitBttn;
    super.scrollToElement(el);
    el.click();
  }
}
