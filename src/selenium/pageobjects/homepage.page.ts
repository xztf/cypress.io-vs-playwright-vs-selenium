import { SeleniumGenericPage } from "../pageobjects/page";
import { ThenableWebDriver } from "selenium-webdriver";
/**
 * Homepage page object class.
 */
export class SeleniumHomepage extends SeleniumGenericPage {
  /** url of the homepage */
  url: string;

  constructor(driver: ThenableWebDriver) {
    super(driver);
    this.url = "https://www.tesena.com";
  }
  /**
   * Visits homepage via `this.url` prop.
   */
  visit(): void {
    super.get(this.url);
  }
}
