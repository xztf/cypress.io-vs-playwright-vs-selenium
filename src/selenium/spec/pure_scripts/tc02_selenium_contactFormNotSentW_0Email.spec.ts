import { expect } from "chai";
import { until, By, WebElement } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC02 - Contact form will not send if email not provided`, function () {
  let sendButton: WebElement;

  before(async function () {
    await this.driver.get("https://www.tesena.com/en/contact");
    // fill name;
    const inputName: WebElement = await this.driver.wait(
      until.elementLocated(By.xpath("//input[contains(@id, 'field-name')]")),
      15000
    );
    // scroll to element to be visible
    await this.driver.executeScript(
      "arguments[0].scrollIntoView();",
      inputName
    );
    await inputName.sendKeys("tester testerovic");
    // fill message
    const textArea: WebElement = await this.driver.wait(
      until.elementLocated(
        By.xpath("//textarea[contains(@id, 'field-message')]")
      ),
      15000
    );
    await textArea.sendKeys("message");
    // click the send button
    sendButton = await this.driver.wait(
      until.elementLocated(By.xpath("//button[@type='submit']")),
      15000
    );
    await sendButton.click();

    // just for show, that inputs were filled
    // await this.driver.sleep(5000);
  });

  it("Send button is still visible", async function () {
    expect(await sendButton.isDisplayed()).to.be.true;
  });

  it("Missinge email error form message is displayed", async function () {
    const message: WebElement = this.driver.wait(
      until.elementLocated(
        By.xpath(
          "//div[@id='field-wrapper-email']//div[contains(@class, 'field-message--error')]"
        )
      ),
      15000
    );
    expect(await message.isDisplayed()).to.be.true;
  });

  /**
   * Test for request not sent is not done in selenium
   * According to e.g https://itnext.io/devtools-protocol-in-selenium-4-6acf89ecb84d, the driver class
   * should have method getDevTools(), which should, for at least chromium, enable to interact with network traffic.
   * However, in the documentation, there is no such thing.
   * https://www.selenium.dev/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_WebDriver.html
   *
   */
});
