import { expect } from "chai";
import { IWebDriverCookie } from "selenium-webdriver";
import { Options } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC04 - __ga cookie is set `, function () {
  let cookie: IWebDriverCookie;

  before(async function () {
    await this.driver.get("https://www.tesena.com");
    const options: Options = await this.driver.manage();
    cookie = await options.getCookie("_ga");
  });

  it("_ga cookie is set", function () {
    expect(cookie).not.to.undefined;
    expect(cookie).to.be.an("object");
    expect(cookie.value).not.to.be.empty;
  });
});
