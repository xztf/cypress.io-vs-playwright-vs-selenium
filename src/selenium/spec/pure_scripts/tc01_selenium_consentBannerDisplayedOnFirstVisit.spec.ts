import { expect } from "chai";
import { WebElement, until, By } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC01 - Consent banner is displayed`, function () {
  before(function () {
    this.driver.get("https://www.tesena.com");
  });

  it("banner accept button is visible", async function () {
    const button: WebElement = this.driver.wait(
      until.elementLocated(
        By.xpath("//button[contains(@class, 'btn-confirm')]")
      ),
      30000
    );
    expect(await button.isDisplayed()).to.be.true;
  });
});
