import { expect } from "chai";
import { By, until, WebElement } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;

/**
 * JavaScript version of selenium library does not have the support
 * for <select> element methods built in yet.
 * So we have to implement a crude - index based work around.
 */
describe(`Browser:: ${BROWSER}: TC09 - Dropdown selector works`, function () {
  let options: Array<WebElement>;

  before(async function () {
    await this.driver.get("https://www.tesena.com/en/integration-test-analyst");
    const select: WebElement = await this.driver.wait(
      until.elementLocated(By.xpath("//select")),
      15000
    );
    options = await select.findElements(By.xpath("//option"));
    // click the fifth one (index == 4), which should be Test Automation Engineer
    // this could also be a test itself, validating, that the options are there
    await options[4]!.click();
    //DEMO - scroll to see and sleep a bit
    // await this.driver.executeScript(
    //   "arguments[0].scrollIntoView();",
    //   options[4]!
    // );
    // await this.driver.sleep(5000);
  });

  it("option 'Test Automation Engineer' was selected", async function () {
    expect(await options[4]!.getText()).to.equal("Test Automation Engineer");
  });
});
