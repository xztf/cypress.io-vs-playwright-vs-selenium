import { expect } from "chai";
import { By, until, WebElement } from "selenium-webdriver";
import { join } from "path";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC08 - Adding file for upload works`, function () {
  before(async function () {
    await this.driver.get("https://www.tesena.com/en/integration-test-analyst");
    const fileEl: WebElement = await this.driver.wait(
      until.elementLocated(By.xpath("//input[@type='file']")),
      15000
    );
    await fileEl.sendKeys(join(process.cwd(), "src/data/testCV.txt"));
  });

  it("Name of the file to be uploaded is displayed", async function () {
    const element: WebElement = await this.driver.wait(
      until.elementLocated(By.xpath('//div[contains(@id, "theFileName")]')),
      15000
    );
    expect(await element.isDisplayed()).to.be.true;
  });
});
