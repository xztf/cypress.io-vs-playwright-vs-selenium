import { expect } from "chai";
import { Alert } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;

/**
 * This looks much simpler, however, selenium will not allow for handling
 * random dialogs. If you do not know, when the popup will come, you are
 * basically ....
 */
describe(`Browser:: ${BROWSER}: TC07 - Alert popup is displayed`, function () {
  let alert: Alert;

  before(async function () {
    await this.driver.get("https://www.tesena.com");
    // inject the alert
    await this.driver.executeScript("alert('test alert, yo!');");
  });

  it("dialog Alert was displayed", async function () {
    // store alert
    alert = await this.driver.switchTo().alert();
    expect(alert).not.to.be.a("undefined");
    // hide by accepting
    await alert.accept();
  });
});
