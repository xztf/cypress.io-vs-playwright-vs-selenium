import { expect } from "chai";
import { By, WebElement } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;
let iframes: Array<WebElement>;

describe(`Browser:: ${BROWSER}: TC05 - YouTube iframes are in the DOM`, function () {
  before(async function () {
    await this.driver.get("https://www.tesena.com/en/insights");
    iframes = await this.driver.findElements(By.xpath("//iframe"));
  });

  it("Iframes are YouTube iframes", async function () {
    expect(iframes).to.be.an("array");

    for (const iframe of iframes) {
      await this.driver.switchTo().frame(iframe);
      const title: string = await this.driver.executeScript(
        "return document.title;"
      );
      expect(title).to.contain("YouTube");
      // we have to switch back to main frame to avoid stale element reference error
      await this.driver.switchTo().defaultContent();
    }
  });
});
