import { expect } from "chai";
import { By, until, WebElement } from "selenium-webdriver";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC05 - click on icon opens LinkedIn`, function () {
  before(async function () {
    await this.driver.get("https://www.tesena.com");
    // consent banner accept banner
    const acceptBttn: WebElement = await this.driver.wait(
      until.elementLocated(
        By.xpath("//button[contains(@class, 'btn-confirm')]")
      ),
      15000
    );
    await this.driver.executeScript(
      "arguments[0].scrollIntoView();",
      acceptBttn
    );
    await acceptBttn.click();
    await this.driver.wait(until.elementIsNotVisible(acceptBttn), 15000);
    // navigate to linkedIn page via click
    // thanks Stack Overflow for this
    const firstWindowHandle: string = await this.driver.getWindowHandle();
    const el: WebElement = await this.driver.wait(
      until.elementLocated(
        By.xpath("//a[contains(@class, 'social-linkedin')]")
      ),
      15000
    );
    await el.click();
    await this.driver.wait(
      async () => (await this.driver.getAllWindowHandles()).length === 2,
      15000
    );
    const handles: Array<string> = await this.driver.getAllWindowHandles();
    handles.forEach(async (handle) => {
      if (handle !== firstWindowHandle) {
        await this.driver.switchTo().window(handle);
      }
    });
    await this.driver.wait(until.titleContains("LinkedIn"), 15000);
  });

  it("click on LinkedIn icon navigated to LinkedIn page", async function () {
    expect(await this.driver.getCurrentUrl()).contains("linkedin.com");
  });
});
