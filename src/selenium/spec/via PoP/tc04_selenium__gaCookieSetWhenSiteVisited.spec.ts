import { expect } from "chai";
import { IWebDriverCookie } from "selenium-webdriver";
import { SeleniumHomepage } from "../../pageobjects/homepage.page";

const BROWSER = process.env.BROWSER;
let homepage: SeleniumHomepage;

describe(`Browser:: ${BROWSER}: TC04 - __ga cookie is set when homepage is visited`, function () {
  this.beforeAll(async function () {
    homepage = new SeleniumHomepage(this.driver);
    homepage.visit();
    await homepage.getCookie("_ga");
  });

  it("_ga cookie is set", function () {
    expect(homepage.cookies.length).equals(1);
    const cookie: IWebDriverCookie | undefined = homepage.cookies[0];
    expect(cookie).not.to.be.undefined;
    expect(cookie).to.be.an("object");
    expect(cookie?.value).not.to.be.empty;
  });
});
