import { expect } from "chai";
import { SeleniumHomepage } from "../../pageobjects/homepage.page";

const BROWSER = process.env.BROWSER;
let homepage: SeleniumHomepage;

describe(`Browser:: ${BROWSER}: TC05 - click on LinkedIn footer icon opens LinkedIn`, function () {
  this.beforeAll(async function () {
    homepage = new SeleniumHomepage(this.driver);
    homepage.visit();
    await homepage.clickConsentBannerAcceptBttn();
    await homepage.navigateToLinkedInViaFooter();
  });

  it("click on LinkedIn icon navigated to LinkedIn page", async function () {
    expect(await homepage.driver.getCurrentUrl()).contains("www.linkedin.com");
  });
});
