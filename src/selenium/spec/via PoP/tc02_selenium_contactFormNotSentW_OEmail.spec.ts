import { expect } from "chai";
import { WebElement } from "selenium-webdriver";
import { SeleniumContactUs } from "../../pageobjects/contactUs.page";

const BROWSER = process.env.BROWSER;
let contactPage: SeleniumContactUs;

describe(`Browser:: ${BROWSER}: TC02 - Contact form will not send if email not provided`, function () {
  this.beforeAll(function () {
    contactPage = new SeleniumContactUs(this.driver);
    contactPage.visit();
    contactPage.fillNameInContactForm("tester testerovic");
    contactPage.fillMessageInContactForm("selenium is weird");
    contactPage.clickContactFormSendBttn();
  });

  it("Send button is still visible", async function () {
    const el: WebElement = contactPage.contactFormSubmitBttn;
    expect(await el.isDisplayed()).to.be.true;
  });

  it("Missing email error form message is displayed", async function () {
    const el: WebElement = contactPage.contactFormEmailInputErrorMsg;
    contactPage.scrollToElement(el);
    expect(await el.isDisplayed()).to.be.true;
  });
});
