import { expect } from "chai";
import { WebElement } from "selenium-webdriver";
import { SeleniumHomepage } from "../../pageobjects/homepage.page";

const BROWSER = process.env.BROWSER;
let homepage: SeleniumHomepage;

describe(`Browser:: ${BROWSER}: TC01 - Consent banner is displayed on first visit`, function () {
  this.beforeAll(function () {
    homepage = new SeleniumHomepage(this.driver);
    homepage.visit();
  });

  it("banner accept button is visible", async function () {
    const el: WebElement = homepage.consentBannerAcceptBttn;
    expect(await el.isDisplayed()).to.be.true;
  });
});
