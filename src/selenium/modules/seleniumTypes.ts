export interface SeleniumCookie {
  domain: string | undefined;
  expiry: Date | number | undefined;
  httpOnly: boolean | undefined;
  name: string;
  path: string | undefined;
  secure: boolean | undefined;
  value: string;
}
