import * as webdriver from "selenium-webdriver";
import * as chrome from "selenium-webdriver/chrome";
import * as firefox from "selenium-webdriver/firefox";
import { MochaHook } from "../../common/commonTypes";

const BROWSER: string = process.env.BROWSER || "chromium";
const HEADLESS: boolean = process.env.HEADLESS === "true" ? true : false;
const VIEWPORT: string =
  process.env.VIEWPORT || JSON.stringify({ width: "1920", height: "1080" });

// if browser is "webkit", then throw, since not supported via selenium
// on my machine
if (BROWSER === "webkit") {
  throw Error("webkit browser not supported.");
}

export const mochaHooks = (): MochaHook => {
  /* Chrome */
  if (BROWSER === "chromium") {
    const chromeOpts: chrome.Options = new chrome.Options();
    HEADLESS && chromeOpts.headless();
    chromeOpts.windowSize(JSON.parse(VIEWPORT));
    chromeOpts.addArguments("--log-level=3");

    return {
      beforeAll: [
        function (): void {
          this.driver = new webdriver.Builder()
            .forBrowser("chrome")
            .setChromeOptions(chromeOpts)
            .build();
        },
      ],
      afterAll: [
        async function (): Promise<void> {
          await this.driver.quit();
        },
      ],
    };
  }
  /* Firefox */
  const firefoxOpts: firefox.Options = new firefox.Options();
  HEADLESS && firefoxOpts.headless();
  firefoxOpts.windowSize(JSON.parse(VIEWPORT));
  firefoxOpts.addArguments("--log-level=3");

  return {
    beforeAll: [
      function (): void {
        this.driver = new webdriver.Builder()
          .forBrowser("firefox")
          .setFirefoxOptions(firefoxOpts)
          .build();
      },
    ],
    afterAll: [
      async function (): Promise<void> {
        await this.driver.quit();
      },
    ],
  };
};
