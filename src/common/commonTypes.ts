export interface MochaHook {
  [index: string]: unknown;
}
