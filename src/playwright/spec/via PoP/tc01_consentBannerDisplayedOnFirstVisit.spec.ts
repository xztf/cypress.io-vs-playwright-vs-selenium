import { expect } from "chai";
import { ElementHandle } from "playwright";
import { Homepage } from "../../pageobjects/homepage.page";

const BROWSER = process.env.BROWSER;
let homepage: Homepage;

describe(`Browser::${BROWSER}: TC01 - Consent banner is displayed on first visit`, function () {
  this.beforeAll(async function () {
    homepage = new Homepage(this.context, this.page);
    await homepage.visit();
  });

  it("banner accept button is visible", async function () {
    const elHndlr: ElementHandle = await homepage.consentBannerAcceptBttn;
    expect(await elHndlr.isVisible()).to.be.true;
  });
});
