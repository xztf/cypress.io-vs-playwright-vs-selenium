import { expect } from "chai";
import { Page } from "playwright";
import { Homepage } from "../../pageobjects/homepage.page";

const BROWSER = process.env.BROWSER;
let homepage: Homepage;
let liPage: Page;

describe(`Browser:: ${BROWSER}: TC05 - click on LinkedIn footer icon opens LinkedIn`, function () {
  this.beforeAll(async function () {
    homepage = new Homepage(this.context, this.page);
    await homepage.visit();
    await homepage.clickConsentBannerAcceptBttn();
    liPage = await homepage.navigateToLinkedInViaFooter();
  });

  it("click on footer LinkedIn icon navigated to LinkedIn page", function () {
    const url: string = liPage.url();
    expect(url).to.be.an("string");
    expect(url).to.contain("www.linkedin.com");
  });
});
