import { expect } from "chai";
import { Homepage } from "../../pageobjects/homepage.page";

const BROWSER = process.env.BROWSER;
const gaRequest = new RegExp(
  /^(https:\/\/)(www\.google-analytics\.com\/)(\w?)(\/?)(collect\?).*$/,
  "g"
);
const container: Array<string> = [];

let homepage: Homepage;

describe(`Browser:: ${BROWSER}: TC03 - GA Requests are sent if consents are provided`, function () {
  this.beforeAll(async function () {
    homepage = new Homepage(this.context, this.page);
    await homepage.visit();
    await homepage.clickConsentBannerAcceptBttn();
    await homepage.wait();
    homepage.listenForRequests(gaRequest, container);
    await homepage.reload();
    await homepage.wait();
  });

  it("GA requests are being sent", function () {
    expect(container).not.to.be.empty;
  });
});
