import { expect } from "chai";
import { ElementHandle } from "playwright";
import { ContactUs } from "../../pageobjects/contactUs.page";

const BROWSER = process.env.BROWSER;
let contactPage: ContactUs;
let status: boolean;

describe(`Browser:: ${BROWSER}: TC02 - Contact Form will not send if email not provided`, function () {
  this.beforeAll(async function () {
    contactPage = new ContactUs(this.context, this.page);
    await contactPage.visit();
    await contactPage.fillNameInContactForm("tester testerovic");
    await contactPage.fillMessageInContactForm(
      "test message - Playwright rulez!"
    );
    [status] = await Promise.all([
      contactPage.isFormSent(),
      contactPage.clickContactFormSendBttn(),
    ]);
  });

  it("Send button is still visible", async function () {
    const elHndlr: ElementHandle = await contactPage.contactFormSubmitBttn;
    expect(await elHndlr.isVisible()).to.be.true;
  });

  it("Missing email error form message is displayed", async function () {
    const elHndlr: ElementHandle =
      await contactPage.contactFormEmailInputErrorMsg;
    expect(await elHndlr.isVisible()).to.be.true;
  });

  it(`Network request is not caught`, async function () {
    expect(status).to.be.false;
  });
});
