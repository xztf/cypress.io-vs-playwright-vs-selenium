import { expect } from "chai";
import { Homepage } from "../../pageobjects/homepage.page";
import { Cookie } from "playwright";

const BROWSER = process.env.BROWSER;
let homepage: Homepage;

describe(`Browser:: ${BROWSER}: TC04 - _ga cookie is set for the 1st party domain when site visited`, function () {
  this.beforeAll(async function () {
    homepage = new Homepage(this.context, this.page);
    await homepage.visit();
    await homepage.getCookies("_ga", ".www.tesena.com");
  });

  it("_ga cookies is set for domain '.www.tesena.com'", function () {
    expect(homepage.cookies.length).equals(
      1,
      "Only one _ga cookie should be set for the domain."
    );
    const cookie: Cookie | undefined = homepage.cookies[0];
    expect(cookie).not.to.be.undefined;
    expect(cookie).to.be.an("object");
    expect(cookie?.value).not.to.be.empty;
  });
});
