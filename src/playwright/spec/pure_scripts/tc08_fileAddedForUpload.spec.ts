import { expect } from "chai";
import { join } from "path";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC08 - Adding file for upload works`, function () {
  before(async function () {
    await this.page.goto("https://www.tesena.com/en/integration-test-analyst", {
      waitUntil: "networkidle",
    });
    await this.page.setInputFiles(
      "//input[@type='file']",
      join(process.cwd(), "src/data/testCV.txt")
    );
  });

  it("Name of the file to be uploaded is displayed", async function () {
    expect(await this.page.isVisible('//div[contains(@id, "theFileName")]')).to
      .be.true;
  });
});
