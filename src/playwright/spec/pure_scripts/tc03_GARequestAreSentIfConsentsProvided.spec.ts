import { expect } from "chai";
import { Request } from "playwright";

const BROWSER = process.env.BROWSER;
const REQUEST = new RegExp(
  /^(https:\/\/)(www\.google-analytics\.com\/)(\w?)(\/?)(collect\?).*$/,
  "g"
);
const CONTAINER: Array<string> = [];

describe(`Browser:: ${BROWSER}: TC03 - GA Requests are sent if consents are provided`, function () {
  before(async function () {
    this.page.on("request", (request: Request) => {
      if (REQUEST.test(request.url())) {
        CONTAINER.push(request.url());
      }
    });
    await this.page.goto("https://www.tesena.com", {
      waitUntil: "networkidle",
    });
    await this.page.click("//button[contains(@class, 'btn-confirm')]");
  });

  it("GA requests are being sent", function () {
    expect(CONTAINER).not.to.be.empty;
  });
});
