import { expect } from "chai";

const BROWSER = process.env.BROWSER;

describe(`Browser::${BROWSER}: TC01 - Consent banner is displayed`, function () {
  before(async function () {
    await this.page.goto("https://www.tesena.com");
  });

  it("banner accept button is visible", async function () {
    expect(
      await this.page.isVisible("//button[contains(@class, 'btn-confirm')]")
    ).to.be.true;
  });
});
