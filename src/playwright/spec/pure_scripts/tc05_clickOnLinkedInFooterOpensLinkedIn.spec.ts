import { expect } from "chai";
import { Page } from "playwright";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC05 - click on icon opens LinkedIn`, function () {
  let linkedInPage: Page;

  before(async function () {
    await this.page.goto("https://www.tesena.com", {
      waitUntil: "networkidle",
    });
    await this.page.click("//button[contains(@class, 'btn-confirm')]");

    [linkedInPage] = await Promise.all([
      this.context.waitForEvent("page"),
      this.page.click("//a[contains(@class, 'social-linkedin')]"),
    ]);
  });

  it("click on LinkedIn footer icon navigated to new LinkedIn page", function () {
    const url: string = linkedInPage.url();
    expect(url).to.be.an("string");
    expect(url).to.contain("linkedin.com");
  });
});
