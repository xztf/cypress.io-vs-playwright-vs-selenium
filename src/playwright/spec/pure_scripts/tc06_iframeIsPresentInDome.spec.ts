import { expect } from "chai";
import { ElementHandle, Frame } from "playwright";

const BROWSER = process.env.BROWSER;
let iframes: Array<ElementHandle>;

describe(`Browser:: ${BROWSER}: TC05 - YouTube iframes are in the DOM`, function () {
  before(async function () {
    await this.page.goto("https://www.tesena.com/en/insights", {
      waitUntil: "networkidle",
    });
    iframes = await this.page.$$("//iframe");
  });

  it("Iframes are YouTube iframes", async function () {
    expect(iframes).to.be.an("array");

    for (const iframe of iframes) {
      const f: Frame | null = await iframe.contentFrame();
      const title: string = await f!.title();
      expect(title).to.contain("YouTube");
    }
  });
});
