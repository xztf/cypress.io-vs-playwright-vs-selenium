import { expect } from "chai";
import { Dialog } from "playwright";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC07 - Alert popup is displayed`, function () {
  let dialogCheck: boolean;

  before(async function () {
    this.page.on("dialog", async (dialog: Dialog) => {
      if (dialog.type() === "alert") {
        dialogCheck = true;
      } else {
        dialogCheck = false;
      }
      await dialog.accept();
    });
    await this.page.goto("https://www.tesena.com/en", {
      waitUntil: "networkidle",
    });
    // add script tag to the page frame with alert popup
    await this.page.addScriptTag({
      content: "alert('test popup, yo!');",
    });
  });

  it("dialog Alert was displayed", function () {
    expect(dialogCheck).to.be.true;
  });
});
