import { expect } from "chai";
import { Page } from "playwright";

const BROWSER = process.env.BROWSER;

const isFormSent = async (page: Page, timeout = 3000): Promise<boolean> => {
  try {
    await page.waitForRequest("https://www.tesena.com/en/contact", {
      timeout: timeout,
    });
    return true;
  } catch (error) {
    return false;
  }
};

describe(`Browser::${BROWSER}: TC02 - Form will not send if email not provided`, function () {
  let status: boolean;

  before(async function () {
    await this.page.goto("https://www.tesena.com/en/contact");
    await this.page.fill(
      "//input[contains(@id, 'field-name')]",
      "tester testerovic"
    );
    await this.page.fill(
      "//textarea[contains(@id, 'field-message')]",
      "just a test message"
    );

    [status] = await Promise.all([
      isFormSent(this.page),
      this.page.click("//button[@type='submit']"),
    ]);
  });

  it("Send button is still visible", async function () {
    expect(await this.page.isVisible("//button[@type='submit']")).to.be.true;
  });

  it("Missing email error msg is displayed", async function () {
    expect(
      await this.page.isVisible(
        "//div[@id='field-wrapper-email']//div[contains(@class, 'field-message--error')]"
      )
    ).to.be.true;
  });

  it("Network request is not caught", async function () {
    expect(status).to.be.false;
  });
});
