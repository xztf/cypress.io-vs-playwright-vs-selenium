import { expect } from "chai";
import { BrowserContext, Cookie } from "playwright";

const BROWSER = process.env.BROWSER;

const getCookies = async (
  context: BrowserContext,
  name?: string,
  domain?: string
): Promise<Array<Cookie>> => {
  const cookies: Array<Cookie> = await context.cookies();

  if (name && domain) {
    const output: Array<Cookie> = [];
    for (const cookie of cookies) {
      if (cookie.name === name && cookie.domain === domain) {
        output.push(cookie);
      }
    }
    return output;
  }

  if (name || domain) {
    const output: Array<Cookie> = [];
    for (const cookie of cookies) {
      if (cookie.name === name || cookie.domain === domain) {
        output.push(cookie);
      }
    }
    return output;
  }
  return cookies;
};

describe(`Browser:: ${BROWSER}: TC04 - _ga cookie is set `, function () {
  let cookies: Array<Cookie>;

  before(async function () {
    await this.page.goto("https://www.tesena.com", {
      waitUntil: "networkidle",
    });
    cookies = await getCookies(this.context, "_ga", ".www.tesena.com");
  });

  it("_ga cookie is set for domain '.www.tesena.com'", function () {
    const cookie: Cookie = cookies[0]!;

    expect(cookie).not.to.be.undefined;
    expect(cookie).to.be.an("object");
    expect(cookie.value).not.to.be.empty;
  });
});
