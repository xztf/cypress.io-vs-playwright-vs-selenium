import { expect } from "chai";
import { ElementHandle } from "playwright";

const BROWSER = process.env.BROWSER;

describe(`Browser:: ${BROWSER}: TC09 - Dropdown selector works`, function () {
  before(async function () {
    await this.page.goto("https://www.tesena.com/en/integration-test-analyst", {
      waitUntil: "networkidle",
    });
    /**
     * This is not good solution, since indexing is brittle.
     * But the front-end solution does not project the chosen option of the selector
     * into the DOM, so we cannot easily check that element in the test :(
     */
    const select: ElementHandle = await this.page.waitForSelector("//select");
    await select.selectOption({ index: 4 });
    //FOR DEMO to actually see something
    // await select.scrollIntoViewIfNeeded();
    // await this.page.waitForTimeout(5000);
  });

  it("option 'Test Automation Engineer' was selected", async function () {
    /**
     * This is not good solution, since indexing is brittle.
     * But the front-end solution does not project the chosen option of the selector
     * into the DOM, so we cannot easily check that element :(
     */
    const options: Array<ElementHandle> = await this.page.$$(
      "//select//option"
    );
    expect(await options[4]!.innerText()).to.equal("Test Automation Engineer");
  });
});
