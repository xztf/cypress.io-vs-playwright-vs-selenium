import { chromium, firefox, webkit } from "playwright";

const BROWSER: string = process.env.BROWSER || "chromium";
const HEADLESS: boolean = process.env.HEADLESS === "true" ? true : false;

export async function mochaGlobalSetup(): Promise<void> {
  this.browserServer = await { chromium, webkit, firefox }[
    BROWSER
  ]!.launchServer({
    headless: HEADLESS,
  });
  process.env.BROWSER_WSENDPOINT = this.browserServer.wsEndpoint();
}

export async function mochaGlobalTeardown(): Promise<void> {
  await this.browserServer.close();
}
