export type WaitUntil = "load" | "domcontentloaded" | "networkidle" | undefined;
