import { chromium, firefox, webkit } from "playwright";
import { MochaHook } from "../../common/commonTypes";

const BROWSER: string = process.env.BROWSER || "chromium";
const VIEWPORT: string =
  process.env.VIEWPORT || JSON.stringify({ width: "1920", height: "1080" });

export const mochaHooks = (): MochaHook => {
  return {
    beforeAll: [
      async function (): Promise<void> {
        this.browser = await { chromium, webkit, firefox }[BROWSER]!.connect({
          wsEndpoint: process.env.BROWSER_WSENDPOINT!,
        });
        this.context = await this.browser.newContext({
          viewport: JSON.parse(VIEWPORT),
        });
        this.page = await this.context.newPage();
      },
    ],
    afterAll: [
      async function (): Promise<void> {
        await this.context.close();
      },
    ],
  };
};
