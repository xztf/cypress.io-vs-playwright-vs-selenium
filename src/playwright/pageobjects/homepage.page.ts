import { Page, BrowserContext } from "playwright";
import { GenericPage } from "./page";

/**
 * Class representing Homepage page object.
 */
export class Homepage extends GenericPage {
  /** URL of the homepage. */
  url: string;

  constructor(context: BrowserContext, page: Page) {
    super(context, page);
    this.url = "https://www.tesena.com";
  }
  /** Opens site homepage via the `url` property. */
  async visit(): Promise<void> {
    await super.goto(this.url);
  }
}
