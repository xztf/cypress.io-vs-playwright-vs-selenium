import {
  Page,
  BrowserContext,
  Response,
  ElementHandle,
  Cookie,
} from "playwright";
import { WaitUntil } from "../modules/playwrightTypes";

/**
 * Generic page class. This class should be never initialized itself.
 * It should be always extended by specific page objects classes.
 * This class should therefore contain all methods, getters and props
 * that can be used accross multiple page objects.
 */
export class GenericPage {
  /** Instance of the playwright's browser's page.  */
  page: Page;
  /** Instance of the playwright's browser contenxt instance. */
  context: BrowserContext;
  /** Xpath selector of the Accept Button of the Consent banner element. */
  consentBannerAcceptBttnSelector: string;
  /**
   * Cookies found in the browser context
   */
  cookies: Array<Cookie>;
  /**
   * Linked In link xpath selector in the footer.
   */
  linkFooterLinkedInSelector: string;

  constructor(context: BrowserContext, page: Page) {
    this.context = context;
    this.page = page;
    this.cookies = [];
    this.consentBannerAcceptBttnSelector =
      "//button[contains(@class, 'btn-confirm')]";
    this.linkFooterLinkedInSelector =
      "//a[contains(@class, 'social-linkedin')]";
  }
  /**
   * Async returns Element Handle of the Accept Bttn of the Consent Banner.
   */
  get consentBannerAcceptBttn(): Promise<ElementHandle> {
    return this.page.waitForSelector(this.consentBannerAcceptBttnSelector, {
      state: "visible",
    });
  }
  /**
   * async gets link of linked in in the footer
   */
  get linkFooterLinkedIn(): Promise<ElementHandle> {
    return this.page.waitForSelector(this.linkFooterLinkedInSelector, {
      state: "visible",
    });
  }

  /**
   * Navigates to specific url. Handles multiple redirects. Wrapper for
   * {@link https://playwright.dev/docs/api/class-page#pagegotourl-options | page.goto()}
   * @param url - url to navigate to
   * @param timeout - defaults to 30000
   * @param waitUntil - "networkidle"
   * @returns `null` | `Response`
   */
  async goto(
    url: string,
    timeout = 30000,
    waitUntil: WaitUntil = "networkidle"
  ): Promise<null | Response> {
    return this.page.goto(url, { timeout: timeout, waitUntil: waitUntil });
  }
  /**
   * Reloads page. Wrapper for
   * {@link https://playwright.dev/docs/api/class-page#pagereloadoptions | page.reload()}
   * @param timeout - defaults to 30000 ms
   * @param waitUntil - defaults to 'networkidle'
   * @returns `null` | `Response`
   */
  async reload(
    timeout = 30000,
    waitUntil: WaitUntil = "networkidle"
  ): Promise<null | Response> {
    return this.page.reload({ timeout: timeout, waitUntil: waitUntil });
  }
  /**
   * Waits for specified time. Wrapper for
   * {@link https://playwright.dev/docs/api/class-page#pagewaitfortimeouttimeout | page.waitForTimeout()}
   * @param timeout - timeout to wait. Defaults to 3000 ms.
   */
  async wait(timeout = 3000): Promise<void> {
    await this.page.waitForTimeout(timeout);
  }
  /**
   * Clicks consent banner accept bttn and waits until button
   * is hidden.
   */
  async clickConsentBannerAcceptBttn(): Promise<void> {
    const elHndlr: ElementHandle = await this.consentBannerAcceptBttn;
    await elHndlr.click({ button: "left" });
  }
  /**
   * Navigates to linked in via footer link
   * @returns instance of the navigated page
   */
  async navigateToLinkedInViaFooter(): Promise<Page> {
    const elHndlr: ElementHandle = await this.linkFooterLinkedIn;
    await elHndlr.scrollIntoViewIfNeeded();
    const [newPage] = await Promise.all([
      this.context.waitForEvent("page"),
      elHndlr.click({ button: "left" }),
    ]);
    await newPage.waitForLoadState();
    return newPage;
  }
  /**
   * Returns cookies found in browser context.
   * You can optionally limit the retunred cookies
   * by providing `name` and `domain` value of expected
   * returned cookie(s)
   * @param name - value of the `name` prop of the Cookie object
   * @param domain - value of the `domain` prop of the Cookie object
   * @returns cookies objects in the `array`. Also assignes found cookies
   * to `this.cookies` prop.
   */
  async getCookies(name?: string, domain?: string): Promise<Array<Cookie>> {
    const cookies: Array<Cookie> = await this.context.cookies();

    if (name && domain) {
      const output: Array<Cookie> = [];
      for (const cookie of cookies) {
        if (cookie.name === name && cookie.domain === domain) {
          output.push(cookie);
        }
      }
      this.cookies = output;
      return output;
    }

    if (name || domain) {
      const output: Array<Cookie> = [];
      for (const cookie of cookies) {
        if (cookie.name === name || cookie.domain === domain) {
          output.push(cookie);
        }
      }
      this.cookies = output;
      return output;
    }

    this.cookies = cookies;
    return cookies;
  }
  /**
   * Listens for specific request `RegExp`. If found, requests' url is
   * pushed to the container.
   * @param req - request to check for in network traffic
   * @param container - array of `string` where matching requests urls
   * are pushed
   */
  listenForRequests(req: RegExp, container: Array<string>): void {
    this.page.on("request", (request) => {
      if (req.test(request.url())) {
        container.push(request.url());
      }
    });
  }
}
