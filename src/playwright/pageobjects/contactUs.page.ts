import { Page, BrowserContext, ElementHandle } from "playwright";
import { GenericPage } from "./page";

/**
 * Class representing Contact Us page object.
 */
export class ContactUs extends GenericPage {
  /** URL of the page. */
  url: string;
  /**
   * Contact form submit bttn xpath selector
   */
  contactFormSubmitBttnSelector: string;
  /**
   * Contact form name input xpath selector
   */
  contactFormNameInputSelector: string;
  /**
   * Contact form text area xpath selector
   */
  contactFormTextAreaSelector: string;
  /**
   * Contact form email input error msg xpath selector
   */
  contactFormEmailInputErrorMsgSelector: string;

  constructor(context: BrowserContext, page: Page) {
    super(context, page);
    this.url = "https://www.tesena.com/en/contact";
    this.contactFormSubmitBttnSelector = "//button[@type='submit']";
    this.contactFormNameInputSelector = "//input[contains(@id, 'field-name')]";
    this.contactFormTextAreaSelector =
      "//textarea[contains(@id, 'field-message')]";
    this.contactFormEmailInputErrorMsgSelector =
      "//div[@id='field-wrapper-email']//div[contains(@class, 'field-message--error')]";
  }
  /**
   * Async gets contact form submit bttn Element Handle
   */
  get contactFormSubmitBttn(): Promise<ElementHandle> {
    return this.page.waitForSelector(this.contactFormSubmitBttnSelector, {
      state: "visible",
    });
  }
  /**
   * Async gets contact form name input Element Handle
   */
  get contactFormNameInput(): Promise<ElementHandle> {
    return this.page.waitForSelector(this.contactFormNameInputSelector, {
      state: "attached",
    });
  }
  /**
   * Async gets contact form text area Element Handle
   */
  get contactFormTextArea(): Promise<ElementHandle> {
    return this.page.waitForSelector(this.contactFormTextAreaSelector, {
      state: "attached",
    });
  }
  /**
   * async gets contact form email input error msg Element Handle
   */
  get contactFormEmailInputErrorMsg(): Promise<ElementHandle> {
    return this.page.waitForSelector(
      this.contactFormEmailInputErrorMsgSelector,
      {
        state: "visible",
      }
    );
  }
  /** Opens the page via the `url` property. */
  async visit(): Promise<void> {
    await super.goto(this.url);
  }
  /**
   * Fills name in contact form input field
   * @param name - name to be filled
   */
  async fillNameInContactForm(name: string): Promise<void> {
    const elHndlr: ElementHandle = await this.contactFormNameInput;
    await elHndlr.fill(name);
  }
  /**
   * Fills message in contact form text area
   * @param message - message to be filled
   */
  async fillMessageInContactForm(message: string): Promise<void> {
    const elHndlr: ElementHandle = await this.contactFormTextArea;
    await elHndlr.fill(message);
  }
  /**
   * Clicks contact form send button
   */
  async clickContactFormSendBttn(): Promise<void> {
    const elHndlr: ElementHandle = await this.contactFormSubmitBttn;
    await elHndlr.click({ button: "left" });
  }
  /**
   * Determines whether form is sent by waiting
   * for request until set timeout
   * @param timeout - maximum time to wait in miliseconds. Defaults to 3000
   */
  async isFormSent(timeout = 3000): Promise<boolean> {
    try {
      await this.page.waitForRequest(this.url, { timeout: timeout });
      return true;
    } catch (error) {
      return false;
    }
  }
}
